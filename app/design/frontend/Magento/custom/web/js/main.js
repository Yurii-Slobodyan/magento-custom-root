define([
        "jquery",
        "select"
    ],
    function($) {
        "use strict";

        $(document).ready(function($){
            $(function () {
                $('select').niceSelect();
            });

            $(window).scroll(function() {
                var scroll = $(window).scrollTop();
                if (scroll > 110) {
                    $(".page-header")
                        .addClass("scroll")
                        .css({"padding": 0})
                }

                else {
                    $(".page-header")
                        .removeClass("scroll")
                        .removeAttr("style")
                }
            });
        });
    });
